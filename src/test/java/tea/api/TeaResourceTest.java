package tea.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import tea.controller.TeaConverter;
import tea.domaine.Tea;
import tea.domaine.TeaRepository;

@Tag("Api")
@SpringBootTest
@AutoConfigureMockMvc
@WithUserDetails("john.smith@invalidemail.com")
public class TeaResourceTest  {

	private static final Long ANY_ID = 1L;
	private static final String ANY_TEANAME = "Red Rose";
	private static final String ANY_TEA_CATEGORY = "Black";
	private static final String ANY_TEA_DESCRIPTION = "Most bland tea ever!";
	private static final String PATH_TO_TEST = "/teas";
	private static final String PATH_WITH_ID = "/" + ANY_ID;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@MockBean
	private TeaRepository repo;
	
	@MockBean 
	private TeaConverter converter;
	
	@Test
	public void get_ifGetSuccessful_thenReturn200Ok() throws Exception {		
		Tea tea = new Tea(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		TeaDto dto1 = new TeaDto(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		Mockito.when(repo.get(ANY_ID)).thenReturn(tea);
		Mockito.when(converter.fromTea(tea)).thenReturn(dto1);
		
		MvcResult result = mockMvc
				.perform(MockMvcRequestBuilders.get(PATH_TO_TEST + "/" + ANY_ID)
						.contentType("application/json"))
		          .andExpect(MockMvcResultMatchers.status().isOk())	          
		          .andReturn();		

		String responseAsString = result.getResponse().getContentAsString();
		String expectedBody = objectMapper.writeValueAsString(dto1);
		Assertions.assertEquals(expectedBody, responseAsString);
	}
	
	@Test
	public void getAll_ifGetSuccessful_thenReturn200Ok() throws Exception {
		Tea tea = new Tea(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		TeaDto dto1 = new TeaDto(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		Mockito.when(repo.get(ANY_ID)).thenReturn(tea);
		Mockito.when(converter.fromTea(tea)).thenReturn(dto1);
		
		mockMvc.perform(MockMvcRequestBuilders.get(PATH_TO_TEST)
		          .contentType("application/json"))
		          .andExpect(MockMvcResultMatchers.status().isOk())	
		          .andReturn();
	}
	
	@Test
	public void create_ifCreateSuccessful_thenReturn201Created() throws Exception {
		TeaDto dto1 = new TeaDto(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		
		mockMvc.perform(MockMvcRequestBuilders.post(PATH_TO_TEST)
		          	.contentType("application/json")
		          	.content(objectMapper.writeValueAsString(dto1)))
		          .andExpect(MockMvcResultMatchers.status().isCreated())
		          .andReturn();
	}	
	
	@Test
	public void update_ifUpdateSuccessfull_thenReturn204NoContent() throws Exception {
		TeaDto dto1 = new TeaDto(ANY_ID, ANY_TEANAME, ANY_TEA_CATEGORY, ANY_TEA_DESCRIPTION);
		
		mockMvc.perform(MockMvcRequestBuilders.put(PATH_TO_TEST)
		          .contentType("application/json")
		          .content(objectMapper.writeValueAsString(dto1)))
		          .andExpect(MockMvcResultMatchers.status().isNoContent())
		          .andReturn();
	}
	
	@Test
	public void delete_ifDeleteSuccessful_thenReturn204NoContent() throws Exception {		
		mockMvc.perform(MockMvcRequestBuilders.delete(PATH_TO_TEST + PATH_WITH_ID)
		          .contentType("application/json"))
		          .andExpect(MockMvcResultMatchers.status().isNoContent())
		          .andReturn();
	}
}
