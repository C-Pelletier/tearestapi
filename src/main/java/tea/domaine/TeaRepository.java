package tea.domaine;

import java.util.List;

public interface TeaRepository {

	void save(Tea tea);

	Tea get(Long id);

	List<Tea> getAll();

	List<Tea> search(String name, String category);

	void delete(Long id);

}
