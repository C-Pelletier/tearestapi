package tea.domaine;

public class Tea {
	
	private Long teaId;
	private String teaName;
	private String teaCategory;
	private String teaDescription;
	
	public Tea(Long teaId, String teaName, String teaCategory, String teaDescription) {
		this.teaId = teaId;
		this.teaName = teaName;
		this.teaCategory = teaCategory;
		this.teaDescription = teaDescription;
	}

	public Long getTeaId() {
		return teaId;
	}

	public String getTeaName() {
		return teaName;
	}

	public String getTeaCategory() {
		return teaCategory;
	}

	public String getTeaDescription() {
		return teaDescription;
	}
}
