package tea.domaine;

public interface UserRepository {
	
	void create(User user);
	
	void save(User user);
	
	User get(String id);
	
	User getByEmail(String email);
}
