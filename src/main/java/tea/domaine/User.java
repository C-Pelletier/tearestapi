package tea.domaine;

public class User {
	
	private String id;
	private String firstname;
	private String lastname;
	private String emailAddress;
	private String password;
	private Role role;
	
	public User(String id, 
			String firstname, 
			String lastname,
			String emailAddress,
			String password,
			Role role) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.emailAddress = emailAddress;
		this.password = password;
		this.role = role;
	}

	public String getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public Role getRole() {
		return role;
	}
	
	public boolean isAdminUser() {
		return role.getRoleName().equalsIgnoreCase("ADMIN");
	}
	
	public boolean isAuthenticatedUser() {
		return role.getRoleName().equalsIgnoreCase("USER");
	}
}
