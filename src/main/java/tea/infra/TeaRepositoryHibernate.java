package tea.infra;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import tea.controller.errors.ObjectNotFoundException;
import tea.domaine.Tea;
import tea.domaine.TeaRepository;

@Repository
public class TeaRepositoryHibernate implements TeaRepository {
	
	private final TeaDao dao;
	private final TeaEntityConverter converter;
	
	public TeaRepositoryHibernate(TeaDao dao, TeaEntityConverter converter) {
		this.dao = dao;
		this.converter = converter;
	}
	
	@Override
	public void save(Tea tea) {
		dao.save(converter.fromTea(tea));
		
	}

	@Override
	public Tea get(Long id) {
		Optional<TeaEntity> entity = dao.findById(id);
		if (entity.isPresent()) {
			return getTeaWithAllInfos(entity.get());
		}
	
		throw new ObjectNotFoundException("Tea with id ("+id+") does not exist.");
	}

	@Override
	public List<Tea> getAll() {
		return dao.findAll().stream()
				.map(entity -> getTeaWithAllInfos(entity))
				.collect(Collectors.toList());
	}

	@Override
	public List<Tea> search(String name, String category) {		
		return dao.getBy(name, category).stream()
				.map(entity -> converter.toTea(entity))
				.collect(Collectors.toList());
	}

	@Override
	public void delete(Long id) {
		dao.deleteById(id);
	}
	
	private Tea getTeaWithAllInfos(TeaEntity entity) {
		Tea tea = converter.toTea(entity);
		//FIXME: Utiliser le Repo TeaInfoRepository pour obtenir la catégorie et la description du thé.
		return tea;
	}
}
