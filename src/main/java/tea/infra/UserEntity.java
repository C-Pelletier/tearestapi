package tea.infra;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class UserEntity {

	@Id
	public String id;
	@Column(length=50, nullable=false)
	public String firstname;
	@Column(length=50, nullable=false)
	public String lastname;
	@Column(length=70, nullable=false)
	public String email;
	@Column(length=70, nullable=false)
	public String password;
	@ManyToOne
	@JoinColumn(name = "role_id")
	public RoleEntity role;
	
	public UserEntity() {		
		
	}

	public UserEntity(String id, String firstname, String lastname, String email, String password, RoleEntity role) {
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.role = role;
	}
}
