package tea.infra;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TeaDao extends JpaRepository<TeaEntity, Long>{

	@Query("SELECT t FROM TeaEntity t WHERE t.teaName = ?1 or t.teaDescription = ?2")
	List<TeaEntity> getBy(String teaName, String teaDescription);
}
