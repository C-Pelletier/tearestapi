package tea.infra;

import org.springframework.stereotype.Component;

import tea.domaine.Tea;

@Component
public class TeaEntityConverter {
	
	public TeaEntity fromTea(Tea tea) {
		return new TeaEntity(tea.getTeaId(), tea.getTeaName(), tea.getTeaCategory(), tea.getTeaDescription());
	}
	
	/**
	 * Le convertisseur retourne volontairement 'null' pour la catégorie et la description, afin de faire
	 * l'intégration avec l'API GraphQL pour aller chercher ces informations.
	 * 
	 * @param entity L'entité à convertir
	 * @return Le thé correspondant, avec l'identifiant et le nom seulement.
	 */
	public Tea toTea(TeaEntity entity) {
		return new Tea(entity.teaId, entity.teaName, null, null);
	}
}
