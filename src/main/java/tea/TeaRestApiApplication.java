package tea;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeaRestApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeaRestApiApplication.class, args);
    }
}
