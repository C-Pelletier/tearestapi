package tea.controller.errors;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import tea.api.ErrorMessageDto;

@ControllerAdvice
@ResponseBody
public class TeaErrorHandler {

	private Logger logger = LoggerFactory.getLogger(TeaErrorHandler.class);
	
	@ExceptionHandler(ObjectNotFoundException.class)
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public ErrorMessageDto resourceNotFoundException(ObjectNotFoundException ex) {
		UUID errorIdentifier = UUID.randomUUID();
		logger.error(LocalDateTime.now().toString() + " [" + errorIdentifier + "]: " + ex.getMessage());
		return new ErrorMessageDto(LocalDateTime.now(), HttpStatus.NOT_FOUND.toString(), errorIdentifier.toString(),
				ex.getMessage());
	}

	@ExceptionHandler(DuplicateException.class)
	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	public ErrorMessageDto invalidInputException(DuplicateException ex) {
		UUID errorIdentifier = UUID.randomUUID();
		logger.error(LocalDateTime.now().toString() + " [" + errorIdentifier + "]: " + ex.getMessage());
		return new ErrorMessageDto(LocalDateTime.now(), HttpStatus.BAD_REQUEST.toString(), errorIdentifier.toString(),
				ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorMessageDto handleValidationExceptions(Exception ex) {
		UUID errorIdentifier = UUID.randomUUID();
		logger.error(LocalDateTime.now().toString() + " [" + errorIdentifier + "]: An unexpected error occurs. "
				+ ex.getMessage() + ", stack:" + ex.toString());
		ex.printStackTrace();
		return new ErrorMessageDto(LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.toString(),
				errorIdentifier.toString(), "An unexpected error occurs.");
	}
}
