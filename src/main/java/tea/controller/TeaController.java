package tea.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RestController;

import tea.api.TeaDto;
import tea.api.TeaResource;
import tea.domaine.TeaRepository;

@RestController
public class TeaController implements TeaResource {
	
	private final TeaRepository repo;
	private final TeaConverter converter;
	
	public TeaController(TeaRepository repo, TeaConverter converter) {
		this.repo = repo;
		this.converter = converter;
	}

	@Override
	public void create(TeaDto teaDto) {
		repo.save(converter.toTea(teaDto));
	}

	@Override
	public List<TeaDto> get() {
		return repo.getAll().stream()
				.map(tea -> converter.fromTea(tea))
				.collect(Collectors.toList());
	}

	@Override
	public TeaDto get(Long id) {
		return converter.fromTea(repo.get(id));
	}

	@Override
	public List<TeaDto> get(String teaName, String teaCategory) {
		return repo.search(teaName, teaCategory).stream()
				.map(tea -> converter.fromTea(tea))
				.collect(Collectors.toList());
	}

	@Override
	public void update(TeaDto tea) {
		repo.save(converter.toTea(tea));
	}

	@Override
	public void delete(Long id) {
		repo.delete(id);
	}
}
