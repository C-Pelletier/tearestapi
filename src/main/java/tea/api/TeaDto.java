package tea.api;

public class TeaDto {
	
	public final Long teaId;
	public final String teaName;
	public final String teaCategory;
	public final String teaDescription;
	
	public TeaDto(Long teaId, String teaName, String teaCategory, String teaDescription) {
		this.teaId = teaId;
		this.teaName = teaName;
		this.teaCategory = teaCategory;
		this.teaDescription = teaDescription;
	}	
}
