insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (1, 'Earl Grey Creme', 'Black', 'Best tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (2, 'Orange Peacock', 'Black', 'Ok tea, for everyday');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (3, 'Lady Grey', 'Black', 'Second best tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (4, 'Irish Breakfast', 'Black', 'Strongest tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (5, 'English Breakfast', 'Black', 'Most popular tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (6, 'Gunpowder', 'Green', 'Most popular green tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (7, 'Roasted almonds', 'Green', 'Most flavorful green tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (8, 'Silk Dragon', 'Green', 'Most powerful green tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (9, 'Jasmine', 'Green', 'Most exotic tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (10, 'Buddha''s Blend', 'White', 'Most zen tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (11, 'Pearl', 'White', 'Most delicate tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (12, 'Vanilla Orchid', 'Oolong', 'Tastiest tea ever!');

insert into TEA_ENTITY (TEA_ID, TEA_NAME, TEA_CATEGORY, TEA_DESCRIPTION)
values (13, 'Magnolia', 'Oolong', 'Richest tea ever!');

insert into ROLE_ENTITY (ID, ROLE_NAME)
values ('1', 'ADMIN');

insert into ROLE_ENTITY (ID, ROLE_NAME)
values ('2', 'USER');

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('46b11dee-0ed5-406e-8180-95e9d6fbdb0b', 'John', 'Smith', 'john.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 1);

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('efc20f79-3499-447d-8e01-949946443e82', 'Jane', 'Smith', 'jane.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 2);

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('7edd3ed5-3d5f-4035-8de2-632d3fb24e7b', 'Kate', 'Smith', 'kate.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 2);

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('9476ee55-fcbc-48fa-a96c-2cc3e2024096', 'Frank', 'Smith', 'frank.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 2);

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('3213a23a-86fb-4b01-9092-a86819cfe2f7', 'Mike', 'Smith', 'mike.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 2);

insert into USER_ENTITY (ID, FIRSTNAME, LASTNAME, EMAIL, PASSWORD, ROLE_ID)
values ('01e6e6b9-0bc8-4df7-bb5f-e7d55d44d573', 'Helen', 'Smith', 'helen.smith@invalidemail.com', '$2y$12$IZoojYcMZkgcST/CUBTQaO0sfPGcBjUsq4U7U8KcKK16eAH7ZgbUC', 2);
